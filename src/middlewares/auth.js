const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
	const authHeader = req.headers.authorization;

	if (!authHeader) return res.status(401).send({ error: 'Nenhum token fornecido.' });

	const parts = authHeader.split(' ');

	if (!parts.length === 2) return res.status(401).send({ error: 'Token não reconhecido.' });

	const [ scheme, token ] = parts;

	if (!/^Bearer$/i.test(scheme)) return res.status(401).send({ error: 'Formatado do Token está incorreto.' });

	jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
		if (err) return res.status(401).send({ error: 'Token inválido.' });

		//req.userId = decoded.id;
		req.user = decoded;

		return next();
	});
};
