const express = require('express');

const controllers = require('./app/controllers');
const authMiddleware = require('./middlewares/auth');

const routes = express.Router();

routes.post('/auth/register', controllers.AuthController.register);
routes.post('/auth/authenticate', controllers.AuthController.authenticate);

routes.use(authMiddleware);

routes.get('/auth/authenticate', controllers.AuthController.show);

/**
 * Punches
 */
routes.get('/punches', controllers.PunchController.index);
routes.get('/punches/:id', controllers.PunchController.show);
routes.post('/punches', controllers.PunchController.store);
routes.put('/punches/:id', controllers.PunchController.update);
routes.delete('/punches/:id', controllers.PunchController.destroy);

/**
 * Employees
 */
routes.get('/employees', controllers.EmployeeController.index);
routes.get('/employees/:id', controllers.EmployeeController.show);
routes.post('/employees', controllers.EmployeeController.store);
routes.put('/employees/:id', controllers.EmployeeController.update);
routes.delete('/employees/:id', controllers.EmployeeController.destroy);

module.exports = routes;
