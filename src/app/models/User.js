const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const UserSchema = new mongoose.Schema({
	chapa: {
		type: String,
		required: true,
		unique: true,
		lowercase: true
	},
	name: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true,
		select: false
	},
	role: {
		type: String,
		required: true,
		enum: [ 'admin', 'estagiario' ]
	},
	createdAt: {
		type: Date,
		default: Date.now
	}
});

UserSchema.pre('save', async function(next) {
	const hash = await bcrypt.hash(this.password, 10);
	this.password = hash;

	next();
});

module.exports = mongoose.model('User', UserSchema);
