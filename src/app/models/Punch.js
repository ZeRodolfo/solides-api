const mongoose = require('mongoose');
const { ObjectId } = mongoose.Schema.Types;

const PunchSchema = new mongoose.Schema({
	date: {
		type: Date,
		required: true
	},
	entry: {
		type: String,
		required: true
	},
	leave: {
		type: String
	},
	interval: {
		type: String
	},
	createdAt: {
		type: Date,
		default: Date.now
	},
	updatedAt: {
		type: Date,
		default: Date.now
	},
	employee: {
		type: ObjectId,
		ref: 'User',
		required: true
	}
});

module.exports = mongoose.model('Punch', PunchSchema);
