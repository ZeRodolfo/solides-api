const User = require('../models/User');

class EmployeeController {
	async index(req, res) {
		const employees = await User.find();

		res.json(employees);
	}

	async show(req, res) {
		const employee = await User.findById(req.params.id);

		res.json(employee);
	}

	async store(req, res) {
		const { chapa } = req.body;
		let user = null;

		try {
			user = await User.findOne({ chapa });

			if (user) {
				return res.status(400).send({ error: 'Usuário já existe.' });
			}

			user = await User.create(req.body);
			user.password = undefined;

			return res.send({ user });
		} catch (err) {
			return res.status(400).send({ error: 'Falha ao registrar Usuário.' });
		}
	}

	async update(req, res) {
		const employee = await User.findByIdAndUpdate(req.params.id, req.body, {
			new: true
		});

		return res.json(employee);
	}

	async destroy(req, res) {
		await User.findByIdAndDelete(req.params.id);

		return res.send();
	}
}

module.exports = new EmployeeController();
