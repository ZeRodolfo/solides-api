const Punch = require('../models/Punch');

class PunchController {
	async index(req, res) {
		const punches = await Punch.find();

		res.json(punches);
	}

	async show(req, res) {
		const punch = await Punch.findById(req.params.id);

		res.json(punch);
	}

	async store(req, res) {
		const { date, entry, leave, interval } = req.body;

		if (date === null || date.trim() === '') {
			return res.status(400).json({ error: 'Data não pode ser vazio.' });
		}

		if (entry === null || entry.trim() === '') {
			return res.status(400).json({ error: 'Horário de Entrada não pode ser vazio.' });
		}

		const punch = await Punch.create({ ...req.body, employee: req.user.id });

		return res.json(punch);
	}

	async update(req, res) {
		const punch = await Punch.findByIdAndUpdate(req.params.id, req.body, {
			new: true
		});

		return res.json(punch);
	}

	async destroy(req, res) {
		await Punch.findByIdAndDelete(req.params.id);

		return res.send();
	}
}

module.exports = new PunchController();
