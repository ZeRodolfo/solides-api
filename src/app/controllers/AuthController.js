const User = require('../models/User');
const generateToken = require('../../globals');
const bcrypt = require('bcryptjs');

class AuthController {
	async register(req, res) {
		const { chapa } = req.body;
		let user = null;

		try {
			user = await User.findOne({ chapa });

			if (user) {
				return res.status(400).send({ error: 'Usuário já existe.' });
			}

			user = await User.create(req.body);
			user.password = undefined;

			return res.send({ user, token: generateToken({ id: user.id, name: user.name, chapa: user.chapa }) });
		} catch (err) {
			return res.status(400).send({ error: 'Falha ao registrar Usuário.' });
		}
	}

	async authenticate(req, res) {
		const { chapa, password } = req.body;

		const user = await User.findOne({ chapa }).select('+password');

		if (!user) return res.status(400).send({ error: 'Usuário não encontrado.' });

		const isValid = await bcrypt.compare(password, user.password);

		if (!isValid) return res.status(400).send({ error: 'Senha inválida.' });

		user.password = undefined;

		res.send({ user, token: generateToken({ id: user.id, name: user.name, chapa: user.chapa }) });
	}

	async show(req, res) {
		const user = await User.findOne({ _id: req.user.id });

		return res.json(user);
	}
}

module.exports = new AuthController();
