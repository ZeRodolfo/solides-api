const jwt = require('jsonwebtoken');

function generateToken(params = {}) {
	return jwt.sign(params, process.env.JWT_SECRET, {
		expiresIn: 86400 // One day expires
	});
}

module.exports = generateToken;
